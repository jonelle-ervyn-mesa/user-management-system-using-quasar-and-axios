
const routes = [
    {
      path: "/",
      component: () => import("layouts/MainLayout.vue"),
      children: [
        { path: "", component: () => import("src/pages/UserInformation.vue") },
      ],
    },
    {
      path: "/",
      component: () => import("layouts/MainLayout.vue"),
      children: [
        {
          path: "user-information",
          name: "user-information",
          component: () => import("pages/UserInformation.vue"),
        },
        {
          path: "list-user",
          name: "list-user",
          component: () => import("pages/ListUser.vue"),
        },
      ],
    },
  
    // Always leave this as last one,
    // but you can also remove it
    {
      path: "/:catchAll(.*)*",
      component: () => import("pages/ErrorNotFound.vue"),
    },
  ];
  
  export default routes;

